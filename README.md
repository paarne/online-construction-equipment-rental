# Online Construction Equipment Rental

A self-service system for renting out construction equipment.

## Setup

### Docker

If you have Docker, you can run the system using:

    docker compose up

Note that the `sql_server` container needs to run before the `web_application` container. If the `web_application` container fails to start you can try to restart it.

After the system is running, you can browse it by navigating to <http://localhost:8080>

### Localhost

If you want to set the system up on your machine directly, without Docker, then you can find the instructions in the [app](https://gitlab.com/paarne/online-construction-equipment-rental/-/blob/main/app/README.md) and [db](https://gitlab.com/paarne/online-construction-equipment-rental/-/blob/main/db/README.md) directories.

Make sure you also have the appropriate [connection string](https://gitlab.com/paarne/online-construction-equipment-rental/-/blob/main/app/UI/appsettings.json) selected in the [application startup file](https://gitlab.com/paarne/online-construction-equipment-rental/-/blob/main/app/UI/Program.cs#L18).

## Use cases

A customer must be able to:

- See the list of rental equipment
- For individual items, enter the number of days for how long they wish to rent it
- Get an invoice

## Inventory

There are three types of equipment available:

- **Heavy** equipment
- **Regular** equipment
- **Specialized** equipment

Example equipment and different types:

| Name                  | Type        |
|-----------------------|-------------|
| Caterpillar bulldozer | Heavy       |
| KamAZ truck           | Regular     |
| Komatsu crane         | Heavy       |
| Volvo steamroller     | Regular     |
| Bosch jackhammer      | Specialized |

## Price calculation

The price of rentals is based on the equipment type and rental length.

There are three different fees:

- **One-time** rental fee – 100€
- **Premium** daily fee – 60€/day
- **Regular** daily fee – 40€/day

The price calculation for different types of equipment:

- **Heavy** – rental price is a one-time fee, plus a premium fee for each day rented.
- **Regular** – rental price is a one-time fee, plus a premium fee for the first 2 days each, plus a regular fee for the number of days exceeding 2 days.
- **Specialized** – rental price is a premium fee for the first 3 days each, plus a regular fee times the number of days exceeding 3 days.

## Loyalty points

Customers get loyalty points when renting equipment. A heavy machine rewards them with 2 points, and other types of equipment grant them one point per rental (regardless of the time rented).

## Invoice

Customers can ask for an invoice that must be generated as an HTML view, pdf, or text file.

The invoice must contain:

- An invoice title
- Lines for every rental item, displaying the name and rental price
- A summary displaying the total price and the number of bonus points earned

Because of a legal requirement, the system must not store intermediate customer balances, only individual machine names. Prices and points must be calculated at the time of invoice generation.
