using FluentAssertions;
using Logic.Common;
using Logic.Customers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using UI.Controllers;
using UI.Models;
using Xunit;

namespace UI.Tests;

public class EquipmentControllerTests
{
    [Fact]
    public void Index_should_return_all_equipment()
    {
        var mock = new Mock<EquipmentRepository>();
        mock
            .Setup(x => x.GetAllEquipment())
            .Returns(new List<Equipment>
            {
                new("Caterpillar bulldozer", "Heavy", 1),
                new("KamAZ truck", "Regular", 2)
            });
        var controller = new EquipmentController(mock.Object);

        var viewResult = (ViewResult)controller.Index();
        var equipmentList = (List<EquipmentViewModel>)viewResult.Model;

        equipmentList.Should().NotBeNull();
        equipmentList.Count.Should().Be(2);

        equipmentList[0].Name.Should().Be("Caterpillar bulldozer");
        equipmentList[0].Type.Should().Be("Heavy");
        equipmentList[0].Id.Should().Be(1);

        equipmentList[1].Name.Should().Be("KamAZ truck");
        equipmentList[1].Type.Should().Be("Regular");
        equipmentList[1].Id.Should().Be(2);
    }
    
    [Fact]
    public void Viewing_rental_item_that_does_not_exist_should_return_error_404()
    {
        var mock = new Mock<EquipmentRepository>();
        mock
            .Setup(x => x.GetById(0))
            .Returns((Maybe<Equipment>)null);
        var controller = new EquipmentController(mock.Object);
        
        var notFoundResult = (NotFoundObjectResult)controller.Rent(0);

        notFoundResult.Should().NotBeNull();
        notFoundResult.StatusCode.Should().Be(404);
        var message = (string)notFoundResult.Value;
        message.Should().Be("No equipment found with ID 0");
    }

    [Fact]
    public void Viewing_rental_item_that_exists_should_return_its_view_model()
    {
        var mock = new Mock<EquipmentRepository>();
        mock
            .Setup(x => x.GetById(1))
            .Returns(new Equipment("Caterpillar bulldozer", "Heavy", 1));
        var controller = new EquipmentController(mock.Object);

        var viewResult = (ViewResult)controller.Rent(1);
        var viewModel = (RentEquipmentViewModel)viewResult.Model;

        viewModel.Should().NotBeNull();
        viewModel.Equipment.Should().NotBeNull();
    }

    [Fact]
    public void Renting_equipment_that_does_not_exist_should_return_error_404()
    {
        var equipmentToRent = new RentEquipmentViewModel
        {
            Equipment = new EquipmentViewModel { Id = 0 },
            Days = 1
        };
        var mock = new Mock<EquipmentRepository>();
        mock
            .Setup(x => x.GetById(0))
            .Returns((Maybe<Equipment>)null);
        var controller = new EquipmentController(mock.Object);

        var notFoundResult = (NotFoundObjectResult)controller.Rent(equipmentToRent);

        notFoundResult.Should().NotBeNull();
        notFoundResult.StatusCode.Should().Be(404);
        var message = notFoundResult.Value.ToString();
        message.Should().NotBeNullOrEmpty();
    }
}
