# Application

## Prerequisites

In order to build and run the application you need:

1. [.NET 8.0 SDK](https://dotnet.microsoft.com/download/dotnet/8.0)
1. A [database](https://gitlab.com/paarne/online-construction-equipment-rental/-/tree/main/db) set up (if you want to run the web UI)

## Using the dotnet CLI

### Building, testing and running

Building the application:

    dotnet build Rental.sln

Running unit tests:

    dotnet test Rental.sln

Running the web UI:

    dotnet run --project UI/UI.csproj

After running the web UI, you can browse it by going to <http://localhost:5061> or <https://localhost:7199>.

If you're using VS Code, more detailed tasks for building, testing and running are defined in the [.vscode/](https://gitlab.com/paarne/online-construction-equipment-rental/-/tree/main/app/.vscode) directory.

### Project managment

Creating a new project, adding it to the solution and adding a reference from the new project to another project:

    dotnet new classlib -n NewProject
    dotnet sln Rental.sln add NewProject/NewProject.csproj
    dotnet add NewProject/NewProject.csproj reference OtherProject/OtherProject.csproj

Adding a NuGet package to a project:

    dotnet add ProjectDir/ProjectName.csproj package PackageName
