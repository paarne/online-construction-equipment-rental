using System.Collections.Generic;
using Logic.Common;

namespace Logic.Customers;

public sealed class RentalLength : ValueObject<RentalLength>
{
    public int Days { get; }

    private RentalLength(int days)
    {
        Days = days;
    }

    public static Result<RentalLength> Create(int days)
    {
        if (days <= 0) return Result.Fail<RentalLength>(
            "The number of days for the rental length needs to be a positive integer.");
        
        return Result.Succeed(new RentalLength(days));
    }

    public int DaysUpTo(int daysUpTo)
    {
        if (Days <= daysUpTo) return Days;
        else return daysUpTo;
    }

    public int DaysExceeding(int daysExceeding)
    {
        if (Days <= daysExceeding) return 0;
        else return Days - daysExceeding;
    }

    public static RentalLength operator +(RentalLength length1, RentalLength length2)
    {
        var sum = new RentalLength(length1.Days + length2.Days);
        return sum;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Days;
    }

    public override string ToString()
    {
        return Days == 1
            ? "1 day"
            : $"{Days} days";
    }
}
