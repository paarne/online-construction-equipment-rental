﻿using Logic.Common;

namespace Logic.Customers;

public class Equipment : Entity
{
    public virtual string Name { get; protected set; }
    public virtual string Type { get; protected set; }

    protected Equipment() { }

    public Equipment(string name, string type, long id = 0)
    {
        Contracts.Require(id >= 0);
        Contracts.Require(name != null);
        Contracts.Require(type != null);

        Id = id;
        Name = name;
        Type = type;
    }

    public override string ToString()
    {
        return $"{Name} ({Type})";
    }
}
