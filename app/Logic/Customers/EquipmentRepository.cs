using System.Collections.Generic;
using System.Linq;
using Logic.Common;
using Logic.Utils;

namespace Logic.Customers;

public class EquipmentRepository : Repository<Equipment>
{
    public virtual IReadOnlyList<Equipment> GetByIds(ICollection<long> equipmentIds)
    {
        using (var session = SessionFactory.OpenSession())
        {
            return session.Query<Equipment>()
                .Where(e => equipmentIds.Contains(e.Id))
                .ToList();
        }
    }

    public virtual IReadOnlyList<Equipment> GetAllEquipment()
    {
        using (var session = SessionFactory.OpenSession())
        {
            return session.Query<Equipment>().ToList();
        }
    }
}
