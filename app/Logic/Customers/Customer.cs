using Logic.Common;

namespace Logic.Customers;

public sealed class Customer
{
    public RentedItems RentedItems { get; private set; }

    public Customer()
    {
        RentedItems = new RentedItems();
    }

    public Customer(RentedItems rentedItems)
    {
        Contracts.Require(rentedItems != null);

        RentedItems = rentedItems;
    }

    public void RentEquipment(Equipment equipment, RentalLength rentalLength)
    {
        Contracts.Require(equipment != null);
        Contracts.Require(rentalLength != null);

        var rentedItem = new RentedItem(equipment, rentalLength);
        RentedItems.Add(rentedItem);
    }
}
