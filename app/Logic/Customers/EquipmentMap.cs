using FluentNHibernate.Mapping;

namespace Logic.Customers;

public class EquipmentMap : ClassMap<Equipment>
{
    public EquipmentMap()
    {
        Id(x => x.Id);
        Map(x => x.Name);
        Map(x => x.Type);
    }
}
