using Logic.Common;
using System.Collections.Generic;
using System.Linq;

namespace Logic.Customers;

public class RentedItems : ValueObject<RentedItems>
{
    private HashSet<RentedItem> ContainedRentedItems { get; }

    public RentedItems()
    {
        ContainedRentedItems = [];
    }

    public RentedItems(IEnumerable<RentedItem> rentedItems)
    {
        Contracts.Require(rentedItems != null);

        ContainedRentedItems = [];

        foreach (var item in rentedItems)
        {
            this.Add(item);
        }
    }

    public void Add(RentedItem rentedItem)
    {
        Contracts.Require(rentedItem != null);

        if (!this.Contains(rentedItem.Equipment))
        {
            ContainedRentedItems.Add(rentedItem);
            return;
        }

        var containedRentedItem = this.GetItemByEquipment(rentedItem.Equipment);
        containedRentedItem.RentalLength += rentedItem.RentalLength;
    }

    public IReadOnlyList<RentedItem> ToList() => ContainedRentedItems.ToList();

    private bool Contains(Equipment equipment) =>
        ContainedRentedItems.SingleOrDefault(x => x.Equipment == equipment) != null;

    private RentedItem GetItemByEquipment(Equipment equipment) =>
        ContainedRentedItems.Single(x => x.Equipment == equipment);

    protected override IEnumerable<object> GetEqualityComponents()
    {
        foreach (var item in ContainedRentedItems)
        {
            yield return item;
        }
    }
}
