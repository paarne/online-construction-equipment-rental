using System;
using System.Collections.Generic;
using Logic.Common;

namespace Logic.Customers;

public sealed class RentedItem : ValueObject<RentedItem>
{
    public Equipment Equipment { get; private set; }
    public RentalLength RentalLength { get; set; }
    public decimal Price => GetPrice();

    public RentedItem(Equipment equipment, RentalLength rentalLength)
    {
        Contracts.Require(equipment != null);
        Contracts.Require(rentalLength != null);
        
        Equipment = equipment;
        RentalLength = rentalLength;
    }

    private decimal GetPrice()
    {
        switch (Equipment.Type)
        {
            case "Heavy":
                return
                    Fee.OneTime
                    + Fee.Premium * RentalLength.Days;

            case "Regular":
                return
                    Fee.OneTime
                    + Fee.Premium * RentalLength.DaysUpTo(2)
                    + Fee.Regular * RentalLength.DaysExceeding(2);

            case "Specialized":
                return
                    Fee.Premium * RentalLength.DaysUpTo(3)
                    + Fee.Regular * RentalLength.DaysExceeding(3);

            default:
                throw new InvalidOperationException("Incorrect equipment type: " + Equipment.Type);
        }
    }

    public override string ToString()
    {
        return $"{Equipment.Name}: {RentalLength}";
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Equipment;
        yield return RentalLength;
    }
}

public static class Fee
{
    /// <summary>
    /// One-time rental fee.
    /// </summary>
    public const int OneTime = 100;
    /// <summary>
    /// Premium daily fee.
    /// </summary>
    public const int Premium = 60;
    /// <summary>
    /// Regular daily fee.
    /// </summary>
    public const int Regular = 40;
}
