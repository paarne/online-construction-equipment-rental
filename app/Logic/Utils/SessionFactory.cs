using System;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Conventions.Instances;
using NHibernate;

namespace Logic.Utils;

public static class SessionFactory
{
    /// <summary>
    /// The configuration used to open and close sessions.
    /// </summary>
    private static ISessionFactory _factory;

    /// <summary>
    /// A session keeps track of all objects loaded from the database into memory and 
    /// automatically updates the corresponding rows in the database according to the changes to 
    /// the objects in memory. A session has the same lifespan as a unit of work, where all 
    /// changes are pushed to the database at the end of its lifetime. Sessions are opened and 
    /// closed frequently.
    /// </summary>
    /// <returns>A session where we have created a connection to the database.</returns>
    /// <remarks>
    /// More info about the unit of work pattern: 
    /// https://martinfowler.com/eaaCatalog/unitOfWork.html
    /// </remarks>
    public static ISession OpenSession()
    {
        return _factory.OpenSession();
    }

    /// <summary>
    /// Set up the configuration in order to use NHibernate.
    /// </summary>
    public static void Init(string connectionString)
    {
        _factory = BuildSessionFactory(connectionString);
    }

    private static ISessionFactory BuildSessionFactory(string connectionString)
    {
        FluentConfiguration configuration = Fluently.Configure()
            .Database(MsSqlConfiguration.MsSql2012.ConnectionString(connectionString))
            .Mappings(m => m.FluentMappings
                .AddFromAssembly(Assembly.GetExecutingAssembly())
                .Conventions.Add(
                    ForeignKey.EndsWith("ID"),
                    ConventionBuilder.Property
                        .When(criteria => criteria.Expect(x => x.Nullable, Is.Not.Set), x => x.Not.Nullable()))
                .Conventions.Add<TableNameConvention>()
                .Conventions.Add<HiLoConvention>()
            );

        try
        {
            return configuration.BuildSessionFactory();
        }
        catch (FluentNHibernate.Cfg.FluentConfigurationException ex)
        {
            if (ex.InnerException.Message.Contains("error: 40 - Could not open a connection to SQL Server"))
                throw new Exception(
                    "Could not connect to SQL Server. If it's not running, you can start it up " + 
                    "using the db/start-sql-server.cmd script. If it is running then see the " + 
                    "inner exception for more details.", ex);

            throw;
        }
    }

    public class TableNameConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            instance.Table($"[dbo].[{instance.EntityType.Name}]");
        }
    }

    /// <summary>
    /// More info about the Hi/Lo algorithm: https://stackoverflow.com/q/282099.
    /// </summary>
    public class HiLoConvention : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            var entityName = instance.EntityType.Name;
            var table = "[dbo].[HiLoNextHighValues]";
            var column = "NextHigh";
            var maxLo = "9";
            var where = $"EntityName = '{entityName}'";

            instance.Column(entityName + "ID");
            instance.GeneratedBy.HiLo(table, column, maxLo, where);
        }
    }
}
