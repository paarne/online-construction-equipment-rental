using System;

namespace Logic.Common;

/// <summary>
/// Represents an operation that can succeed or fail.
/// </summary>
public class Result
{
    public bool IsSuccess { get; }
    public bool IsFailure => !IsSuccess;
    public string Error { get; }

    protected Result(bool isSuccess, string error)
    {
        if (isSuccess && !string.IsNullOrEmpty(error))
            throw new InvalidOperationException("Result cannot succeed and have an error at the same time.");
        if (!isSuccess && string.IsNullOrEmpty(error))
            throw new InvalidOperationException("Result cannot fail without an error.");

        IsSuccess = isSuccess;
        Error = error;
    }

    public static Result Succeed()
    {
        return new Result(true, string.Empty);
    }

    public static Result<T> Succeed<T>(T value)
    {
        return new Result<T>(value, true, string.Empty);
    }

    public static Result Fail(string message)
    {
        return new Result(false, message);
    }

    public static Result<T> Fail<T>(string message)
    {
        return new Result<T>(default(T), false, message);
    }
}

/// <example>
/// Even though <see cref="Result{T}"/> might behave similarly to <see cref="Maybe{T}"/>, they 
/// have different meanings. For example, a connection to the database may fail, so you'd use 
/// <see cref="Result{T}"/> but even when it succeeds it might not return any meaningful value, so 
/// you'd also use <see cref="Maybe{T}"/>. In this case, <see cref="Result{Maybe{T}}"/> is a 
/// legitimate return value.
/// </example>
public class Result<T> : Result
{
    private readonly T _value;
    public T Value
    {
        get
        {
            if (!IsSuccess)
                throw new InvalidOperationException("Cannot get the value of a failed result.");

            return _value;
        }
    }

    protected internal Result(T value, bool isSuccess, string error)
        : base(isSuccess, error)
    {
        _value = value;
    }
}
