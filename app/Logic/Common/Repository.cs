using Logic.Utils;

namespace Logic.Common;

public abstract class Repository<T> where T : Entity
{
    public virtual Maybe<T> GetById(long id)
    {
        using (var session = SessionFactory.OpenSession())
        {
            return session.Get<T>(id);
        }
    }

    public virtual void Save(T entity)
    {
        using (var session = SessionFactory.OpenSession())
        using (var transaction = session.BeginTransaction())
        {
            session.SaveOrUpdate(entity);
            transaction.Commit();
        }
    }
}
