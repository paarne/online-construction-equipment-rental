using System;
using System.Diagnostics;

namespace Logic.Common;

/// <summary>
/// A specification (like a business contract) used to check that all specified preconditions, in 
/// order to invoke an operation, are met.
/// </summary>
/// <remarks>
/// More info on design by contract:
/// https://en.wikipedia.org/wiki/Design_by_contract
/// </remarks>
public static class Contracts
{
    [DebuggerStepThrough]
    public static void Require(bool precondition, string message = "")
    {
        if (!precondition)
            throw new ContractException(message);
    }
}

/// <summary>
/// Used in case a precondition is violated.
/// </summary>
public class ContractException : Exception
{
    public ContractException()
        : base() { }

    public ContractException(string message)
        : base(message) { }

    public ContractException(string message, Exception innerException)
        : base(message, innerException) { }
}
