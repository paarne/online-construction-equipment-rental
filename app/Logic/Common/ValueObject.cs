using System.Collections.Generic;
using System.Linq;

namespace Logic.Common;

/// <summary>
/// Base class for all Value Objects. Value Objects differ from Entities in that they have 
/// structural equality. This means that two Value Objects are equal if all their members match. 
/// Value Objects do not have a corresponding table in a database.
/// </summary>
/// <remarks>
/// More info on Entities and Value Objects:
/// https://enterprisecraftsmanship.com/posts/entity-vs-value-object-the-ultimate-list-of-differences
/// </remarks>
public abstract class ValueObject<T> where T : ValueObject<T>
{
    protected abstract IEnumerable<object> GetEqualityComponents();

    public override bool Equals(object obj)
    {
        var valueObject = obj as T;

        if (ReferenceEquals(valueObject, null))
            return false;

        return EqualsCore(valueObject);
    }

    private bool EqualsCore(ValueObject<T> other)
    {
        return GetEqualityComponents().SequenceEqual(other.GetEqualityComponents());
    }

    public override int GetHashCode()
    {
        return GetEqualityComponents()
            .Aggregate(1, (current, obj) => current * 23 + (obj?.GetHashCode() ?? 0));
    }

    public static bool operator ==(ValueObject<T> a, ValueObject<T> b)
    {
        if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            return true;

        if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(ValueObject<T> a, ValueObject<T> b)
    {
        return !(a == b);
    }
}
