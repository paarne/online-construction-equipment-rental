using System;
using NHibernate.Proxy;

namespace Logic.Common;

/// <summary>
/// Base class for all Entities. Entities differ from Value Objects in that they have a unique 
/// identifier (<see cref="Id"/>) and usually have a corresponding table in a database.
/// </summary>
/// <remarks>
/// More info on Entities and Value Objects:
/// https://enterprisecraftsmanship.com/posts/entity-vs-value-object-the-ultimate-list-of-differences
/// </remarks>
/// <example>
/// Unfortunately, in order to use NHibernate as the ORM, we need to leak some persistance 
/// concerns into our domain model. NHibernate creates proxy classes on top of our entities and 
/// overrides all non private members in them. This means that all entities:
///     1. Need to be unsealed
///     2. Need to have all non private members marked as virtual
///     3. Cannot have private setters (use protected instead)
///     4. Need to have a parameterless constructor (can be made protected)
/// </example>
public abstract class Entity
{
    public virtual long Id { get; protected set; }

    public override bool Equals(object obj)
    {
        var other = obj as Entity;

        if (ReferenceEquals(other, null))
            return false;

        if (ReferenceEquals(this, other))
            return true;

        if (GetRealType() != other.GetRealType())
            return false;

        if (Id == 0 || other.Id == 0)
            return false;

        return Id == other.Id;
    }

    public static bool operator ==(Entity a, Entity b)
    {
        if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            return true;

        if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(Entity a, Entity b)
    {
        return !(a == b);
    }

    public override int GetHashCode()
    {
        return (GetRealType().ToString() + Id).GetHashCode();
    }

    /// <summary>
    /// Get this class' type without initializing NHibernate's proxy.
    /// </summary>
    /// <remarks>
    /// We don't use <see cref="object.GetType"/> as NHibernate creates proxy classes that 
    /// inherit from our classes. This means that <see cref="object.GetType"/> would return the 
    /// type of the proxy class. We need to override this and get the type without initializing 
    /// the proxy.
    /// </remarks>
    private Type GetRealType()
    {
        return NHibernateProxyHelper.GetClassWithoutInitializingProxy(this);
    }
}
