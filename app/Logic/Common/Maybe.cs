using System;

namespace Logic.Common;

/// <summary>
/// Represents a nullable (optional) value.
/// </summary>
/// <remarks>
/// Also called option.
/// </remarks>
/// <example>
/// Wraps the value of a reference type (<see cref="T"/>) to indicate that it's nullable.
/// For example:
/// <code>
/// Maybe{string} nullableStringWithValue = "Value";
/// Maybe{string} nullableStringWithoutValue = null;
/// </code>
/// </example>
public struct Maybe<T> : IEquatable<Maybe<T>>
    where T : class
{
    private readonly T _value;
    public T Value =>
        HasValue
            ? _value
            : throw new InvalidOperationException(
                $"Cannot access the value of {typeof(T).Name} as it has no value");

    public bool HasValue => _value != null;
    public bool HasNoValue => !HasValue;

    private Maybe(T value)
    {
        _value = value;
    }

    public static implicit operator Maybe<T>(T value)
    {
        return new Maybe<T>(value);
    }

    public static bool operator ==(Maybe<T> maybe, T value)
    {
        if (maybe.HasNoValue)
            return false;

        return maybe.Value.Equals(value);
    }

    public static bool operator !=(Maybe<T> maybe, T value)
    {
        return !(maybe == value);
    }

    public static bool operator ==(Maybe<T> first, Maybe<T> second)
    {
        return first.Equals(second);
    }

    public static bool operator !=(Maybe<T> first, Maybe<T> second)
    {
        return !(first == second);
    }

    public override bool Equals(object obj)
    {
        if (!(obj is Maybe<T>))
            return false;

        var other = (Maybe<T>)obj;
        return Equals(other);
    }

    public bool Equals(Maybe<T> other)
    {
        if (HasNoValue && other.HasNoValue)
            return true;

        if (HasNoValue || other.HasNoValue)
            return false;

        return _value.Equals(other._value);
    }

    public override int GetHashCode() => _value.GetHashCode();

    public override string ToString()
    {
        return HasValue
            ? _value.ToString()
            : "No Value";
    }

    /// <summary>
    /// Converts <see cref="Maybe{T}"/> back into a reference type value.
    /// </summary>
    /// <remarks>
    /// Useful eg. for converting a Maybe type back into a null outside our domain model, if we 
    /// want to show it in a view or save it to a database.
    /// </remarks>
    public T Unwrap()
    {
        return HasValue
            ? _value
            : default(T);
    }
}
