using FluentAssertions;
using Logic.Customers;
using Xunit;

namespace Logic.Tests;

public class RentedItemSpecs
{
    [Fact]
    public void Two_rented_item_instances_equal_with_same_equipment_and_length()
    {
        var equipment1 = new Equipment("Bulldozer", "Heavy", 1);
        var equipment2 = new Equipment("Bulldozer", "Heavy", 1);
        var rentalLength1 = RentalLength.Create(2).Value;
        var rentalLength2 = RentalLength.Create(2).Value;

        var rentedItem1 = new RentedItem(equipment1, rentalLength1);
        var rentedItem2 = new RentedItem(equipment2, rentalLength2);

        rentedItem1.Should().Be(rentedItem2);
    }

    [Fact]
    public void Two_rented_item_instances_dont_equal_with_different_equipment_and_length()
    {
        var equipment1 = new Equipment("Bulldozer", "Heavy");
        var equipment2 = new Equipment("Crane", "Heavy");
        var rentalLength1 = RentalLength.Create(2).Value;
        var rentalLength2 = RentalLength.Create(3).Value;

        var rentedItem1 = new RentedItem(equipment1, rentalLength1);
        var rentedItem2 = new RentedItem(equipment2, rentalLength2);

        rentedItem1.Should().NotBe(rentedItem2);
    }

    [Theory]
    [InlineData("Heavy", 1, 160)]
    [InlineData("Heavy", 2, 220)]
    [InlineData("Heavy", 3, 280)]
    [InlineData("Heavy", 4, 340)]
    [InlineData("Heavy", 5, 400)]
    [InlineData("Regular", 1, 160)]
    [InlineData("Regular", 2, 220)]
    [InlineData("Regular", 3, 260)]
    [InlineData("Regular", 4, 300)]
    [InlineData("Regular", 5, 340)]
    [InlineData("Specialized", 1, 60)]
    [InlineData("Specialized", 2, 120)]
    [InlineData("Specialized", 3, 180)]
    [InlineData("Specialized", 4, 220)]
    [InlineData("Specialized", 5, 260)]
    [InlineData("Specialized", 6, 300)]
    public void Rental_price_is_calculated_correctly(string equipmentType, int rentalLengthDays, decimal expectedPrice)
    {
        var equipment = new Equipment("Test equipment", equipmentType);
        var rentalLength = RentalLength.Create(rentalLengthDays).Value;

        var rentedItem = new RentedItem(equipment, rentalLength);

        rentedItem.Price.Should().Be(expectedPrice);
    }
}
