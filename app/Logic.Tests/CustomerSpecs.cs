﻿using FluentAssertions;
using Logic.Customers;
using Xunit;

namespace Logic.Tests;

public class CustomerSpecs
{
    [Fact]
    public void New_customer_should_have_no_rented_equipment()
    {
        var customer = new Customer();

        customer.RentedItems.ToList().Should().BeEmpty();
    }

    [Fact]
    public void Renting_equipment_adds_to_rented_equipment()
    {
        var customer = new Customer();
        var rentalLength = RentalLength.Create(7).Value;
        var equipment = new Equipment("Caterpillar bulldozer", "Heavy");

        customer.RentEquipment(equipment, rentalLength);

        customer.RentedItems.ToList().Should().HaveCount(1);
        customer.RentedItems.ToList().Should().ContainSingle(
            x => x.Equipment == equipment &&
            x.RentalLength == rentalLength);
    }
}
