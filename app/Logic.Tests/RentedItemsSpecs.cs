using FluentAssertions;
using Logic.Customers;
using System.Collections.Generic;
using Xunit;

namespace Logic.Tests;

public class RentedItemsSpecs
{
    [Fact]
    public void Two_rented_items_instances_equal_with_same_rented_items()
    {
        var equipment1 = new Equipment("Bulldozer", "Heavy", 1);
        var equipment2 = new Equipment("Crane", "Heavy", 2);
        var rentalLength1 = RentalLength.Create(1).Value;
        var rentalLength2 = RentalLength.Create(2).Value;
        var rentedItemsList1 = new List<RentedItem>
            {
                new RentedItem(equipment1, rentalLength1),
                new RentedItem(equipment2, rentalLength2)
            };
        var rentedItemsList2 = new List<RentedItem>
            {
                new RentedItem(equipment1, rentalLength1),
                new RentedItem(equipment2, rentalLength2)
            };

        var rentedItems1 = new RentedItems(rentedItemsList1);
        var rentedItems2 = new RentedItems(rentedItemsList2);

        rentedItems1.Should().Be(rentedItems2);
    }

    [Fact]
    public void Two_rented_items_instances_dont_equal_with_different_rented_items()
    {
        var equipment1 = new Equipment("Bulldozer", "Heavy", 1);
        var equipment2 = new Equipment("Crane", "Heavy", 2);
        var rentalLength1 = RentalLength.Create(1).Value;
        var rentalLength2 = RentalLength.Create(2).Value;
        var rentedItemsList1 = new List<RentedItem>
            {
                new RentedItem(equipment1, rentalLength1),
                new RentedItem(equipment2, rentalLength2)
            };
        var rentedItemsList2 = new List<RentedItem>
            {
                new RentedItem(equipment1, rentalLength1)
            };

        var rentedItems1 = new RentedItems(rentedItemsList1);
        var rentedItems2 = new RentedItems(rentedItemsList2);

        rentedItems1.Should().NotBe(rentedItems2);
    }

    [Fact]
    public void Renting_same_equipment_sums_rental_days()
    {
        var equipment = new Equipment("Caterpillar bulldozer", "Heavy");
        var rentedItems = new RentedItems(new List<RentedItem>
            {
                new RentedItem(equipment, RentalLength.Create(2).Value)
            });

        rentedItems.Add(new RentedItem(equipment, RentalLength.Create(5).Value));

        rentedItems.ToList().Should().HaveCount(1);
        rentedItems.ToList().Should().ContainSingle(
            x => x.Equipment == equipment &&
            x.RentalLength.Days == 7);
    }

    [Fact]
    public void Duplicate_rented_equipment_sums_rental_days()
    {
        var equipment = new Equipment("Caterpillar bulldozer", "Heavy");
        var rentedItemsList = new List<RentedItem>
            {
                new RentedItem(equipment, RentalLength.Create(2).Value),
                new RentedItem(equipment, RentalLength.Create(5).Value)
            };

        var rentedItems = new RentedItems(rentedItemsList);

        rentedItems.ToList().Should().HaveCount(1);
        rentedItems.ToList().Should().ContainSingle(
            x => x.Equipment == equipment &&
            x.RentalLength.Days == 7);
    }
}
