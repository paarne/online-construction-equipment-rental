using FluentAssertions;
using Logic.Customers;
using Xunit;

namespace Logic.Tests;

public class RentalLengthSpecs
{
    [Fact]
    public void New_rental_length_has_correct_nr_of_days()
    {
        var days = 1;

        var rentalLength = RentalLength.Create(days).Value;

        rentalLength.Days.Should().Be(days);
    }

    [Fact]
    public void Two_rental_length_instances_equal_with_same_nr_of_days()
    {
        var rentalLength1 = RentalLength.Create(2).Value;
        var rentalLength2 = RentalLength.Create(2).Value;

        rentalLength1.Should().Be(rentalLength2);
    }

    [Fact]
    public void Two_rental_length_instances_dont_equal_with_different_nr_of_days()
    {
        var rentalLength1Day = RentalLength.Create(1).Value;
        var rentalLength2Days = RentalLength.Create(2).Value;

        rentalLength1Day.Should().NotBe(rentalLength2Days);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-1)]
    public void Cannot_create_rental_length_with_invalid_days(int days)
    {
        var rentalLengthResult = RentalLength.Create(days);

        rentalLengthResult.IsFailure.Should().BeTrue();
    }

    [Theory]
    [InlineData(1, 1, 1)]
    [InlineData(1, 2, 1)]
    [InlineData(2, 1, 1)]
    [InlineData(3, 1, 1)]
    [InlineData(3, 2, 2)]
    public void DaysUpTo_produces_correct_result(int rentalLengthDays, int daysUpTo, int expectedDays)
    {
        var rentalLength = RentalLength.Create(rentalLengthDays).Value;

        var days = rentalLength.DaysUpTo(daysUpTo);

        days.Should().Be(expectedDays);
    }

    [Theory]
    [InlineData(1, 1, 0)]
    [InlineData(1, 2, 0)]
    [InlineData(2, 1, 1)]
    [InlineData(3, 1, 2)]
    [InlineData(3, 2, 1)]
    public void DaysExceeding_produces_correct_result(int rentalLengthDays, int daysExceeding, int expectedDays)
    {
        var rentalLength = RentalLength.Create(rentalLengthDays).Value;

        var days = rentalLength.DaysExceeding(daysExceeding);

        days.Should().Be(expectedDays);
    }
}
