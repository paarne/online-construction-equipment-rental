﻿using System.Text.Json;

namespace UI.Utils;

/// <summary>
/// A place to hold extension methods.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Saves <paramref name="value"/> into the session.
    /// </summary>
    public static void Set<T>(this ISession session, string key, T value)
    {
        var json = JsonSerializer.Serialize(value);
        session.SetString(key, json);
    }

    /// <summary>
    /// Retrieves <typeparamref name="T"/> from the session.
    /// </summary>
    public static T Get<T>(this ISession session, string key)
    {
        var json = session.GetString(key);
        return json == null ? default : JsonSerializer.Deserialize<T>(json);
    }
}
