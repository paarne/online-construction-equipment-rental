using Logic.Common;
using Logic.Customers;

namespace UI.Utils;

/// <summary>
/// Saves and retrieves Customer data from the HTTP session.
/// </summary>
public class CustomerRepository
{
    private readonly HttpContext _httpContext;
    private readonly EquipmentRepository _equipmentRepository;
    private static readonly string _key = "Customer";

    public CustomerRepository(HttpContext httpContext)
    {
        Contracts.Require(httpContext != null);

        _httpContext = httpContext;
        _equipmentRepository = new EquipmentRepository();
    }

    public Customer GetCustomer()
    {
        var customerDto = _httpContext.Session.Get<CustomerDto>(_key);
        if (customerDto == null)
            return new Customer();

        var rentedItems = new RentedItems();
        var customerEquipmentIds = customerDto.RentedItems.Select(x => x.EquipmentId).ToList();
        var customerEquipment = _equipmentRepository.GetByIds(customerEquipmentIds);

        foreach (var item in customerDto.RentedItems)
        {
            var equipment = customerEquipment.Single(e => e.Id == item.EquipmentId);
            var rentalLength = RentalLength.Create(item.RentalLengthDays).Value;
            rentedItems.Add(new RentedItem(equipment, rentalLength));
        }

        return new Customer(rentedItems);
    }

    public void SaveCustomer(Customer customer)
    {
        var customerDto = new CustomerDto(customer);
        _httpContext.Session.Set(_key, customerDto);
    }

    private class CustomerDto
    {
        public List<RentedItemDto> RentedItems { get; set; }

        public CustomerDto() { }

        public CustomerDto(Customer customer)
        {
            RentedItems = customer.RentedItems.ToList().Select(x => new RentedItemDto(x)).ToList();
        }
    }

    private class RentedItemDto
    {
        public long EquipmentId { get; set; }
        public int RentalLengthDays { get; set; }

        public RentedItemDto() { }

        public RentedItemDto(RentedItem item)
        {
            EquipmentId = item.Equipment.Id;
            RentalLengthDays = item.RentalLength.Days;
        }
    }
}
