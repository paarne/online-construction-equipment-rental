using Logic.Customers;

namespace UI.Models;

public class RentEquipmentViewModel
{
    public EquipmentViewModel Equipment { get; set; }
    public int? Days { get; set; }

    public RentEquipmentViewModel() {}

    public RentEquipmentViewModel(Equipment equipment, int? daysToRent=null)
    {
        Equipment = new EquipmentViewModel(equipment);
        Days = daysToRent;
    }
}
