using Logic.Customers;

namespace UI.Models;

public class RentedItemViewModel
{
    public RentEquipmentViewModel RentedItem { get; set; }
    public decimal Price { get; set; }

    public RentedItemViewModel() { }

    public RentedItemViewModel(RentedItem item)
    {
        RentedItem = new RentEquipmentViewModel(item.Equipment, item.RentalLength.Days);
        Price = item.Price;
    }
}
