using Logic.Customers;

namespace UI.Models;

public class CustomerViewModel
{
    public IList<RentedItemViewModel> RentedItems { get; set; }

    public CustomerViewModel() { }

    public CustomerViewModel(Customer customer)
    {
        RentedItems = customer.RentedItems.ToList().Select(x => new RentedItemViewModel(x)).ToList();
    }
}
