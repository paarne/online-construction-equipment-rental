using Logic.Customers;

namespace UI.Models;

public class EquipmentViewModel
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }

    public EquipmentViewModel() {}

    public EquipmentViewModel(Equipment equipment)
    {
        Id = equipment.Id;
        Name = equipment.Name;
        Type = equipment.Type;
    }
}
