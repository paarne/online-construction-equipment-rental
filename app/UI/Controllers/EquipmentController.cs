using Microsoft.AspNetCore.Mvc;
using Logic.Customers;
using UI.Models;
using UI.Utils;

namespace UI.Controllers;

public class EquipmentController : Controller
{
    private readonly EquipmentRepository _equipmentRepo;

    public EquipmentController(EquipmentRepository equipmentRepository)
    {
        _equipmentRepo = equipmentRepository;
    }

    [HttpGet]
    public IActionResult Index()
    {
        var equipment = _equipmentRepo.GetAllEquipment();
        var viewModels = equipment
            .Select(x => new EquipmentViewModel(x))
            .ToList();

        return View(viewModels);
    }

    [HttpGet]
    public IActionResult Rent(int id)
    {
        var equipmentToRent = _equipmentRepo.GetById(id);
        if (equipmentToRent.HasNoValue)
            return NotFound("No equipment found with ID " + id);

        var viewModel = new RentEquipmentViewModel(equipmentToRent.Value);

        return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Rent(RentEquipmentViewModel rentEquipmentViewModel)
    {
        if (!rentEquipmentViewModel.Days.HasValue)
        {
            ModelState.AddModelError("Days", "Enter the no of days to rent the equipment");
            return View(rentEquipmentViewModel);
        }

        var rentalLengthResult = RentalLength.Create(rentEquipmentViewModel.Days.Value);
        if (rentalLengthResult.IsFailure)
        {
            ModelState.AddModelError("Days", rentalLengthResult.Error);
            return View(rentEquipmentViewModel);
        }

        var equipmentId = rentEquipmentViewModel.Equipment.Id;
        var equipmentToRent = _equipmentRepo.GetById(equipmentId);
        if (equipmentToRent.HasNoValue)
            return NotFound("No equipment found with ID " + equipmentId);

        var customerRepo = new CustomerRepository(HttpContext);
        var customer = customerRepo.GetCustomer();

        customer.RentEquipment(equipmentToRent.Value, rentalLengthResult.Value);

        customerRepo.SaveCustomer(customer);

        return RedirectToAction("Index");
    }
}
