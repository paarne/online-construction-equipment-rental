using Microsoft.AspNetCore.Mvc;
using UI.Utils;
using UI.Models;

namespace UI.Controllers;

public class RentedItemsController : Controller
{
    public RentedItemsController()
    {
    }

    public IActionResult Index()
    {
        var customerRepo = new CustomerRepository(HttpContext);
        var customer = customerRepo.GetCustomer();
        var customerVm = new CustomerViewModel(customer);

        return View(customerVm);
    }
}
