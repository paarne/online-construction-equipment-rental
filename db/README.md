# Database

## Prerequisites

In order to run the database you need:

1. [SQL Server 2019 Developer](https://go.microsoft.com/fwlink/?linkid=866662)

## Setting up the database

Set up the database and tables by running the following scripts in this order:

1. create-db.sql
1. create-tables.sql
1. insert-data.sql
