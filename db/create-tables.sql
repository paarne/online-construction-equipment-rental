/* In case something went wrong:
DROP TABLE dbo.Equipment;
DROP TABLE dbo.HiLoNextHighValues;
*/

USE Rental;
GO

-- Equipment table

IF OBJECT_ID('dbo.Equipment', 'U') IS NULL
BEGIN
    CREATE TABLE dbo.Equipment (
        EquipmentID BIGINT PRIMARY KEY CLUSTERED,
        Name NVARCHAR(255) NOT NULL,
        Type NVARCHAR(255) NOT NULL
    );

    PRINT '';
    PRINT 'Table "dbo.Equipment" created';
END
ELSE
BEGIN
    PRINT '';
    PRINT 'Table "dbo.Equipment" already exists';
END

PRINT '';
PRINT 'SELECT * FROM dbo.Equipment';
SELECT * FROM dbo.Equipment;

-- HiLoNextHighValues table

IF OBJECT_ID('dbo.HiLoNextHighValues', 'U') IS NULL
BEGIN
    CREATE TABLE dbo.HiLoNextHighValues (
        EntityName NVARCHAR(255) NOT NULL UNIQUE,
        NextHigh INT NOT NULL
    );

    PRINT '';
    PRINT 'Table "dbo.HiLoNextHighValues" created';
END
ELSE
BEGIN
    PRINT '';
    PRINT 'Table "dbo.HiLoNextHighValues" already exists';
END

PRINT '';
PRINT 'SELECT * FROM dbo.HiLoNextHighValues';
SELECT * FROM dbo.HiLoNextHighValues;
