/* In case something went wrong:
DELETE FROM dbo.Equipment;
DELETE FROM dbo.HiLoNextHighValues;
*/

USE Rental;
GO

-- Equipment table

DECLARE @EquipmentToInsert TABLE (
    Id BIGINT NOT NULL,
    Name NVARCHAR(255) NOT NULL,
    Type NVARCHAR(255) NOT NULL
);

PRINT '';
PRINT 'INSERT INTO @EquipmentToInsert';
INSERT INTO @EquipmentToInsert VALUES
    (1, 'Caterpillar bulldozer', 'Heavy'),
    (2, 'KamAZ truck', 'Regular'),
    (3, 'Komatsu crane', 'Heavy'),
    (4, 'Volvo steamroller', 'Regular'),
    (5, 'Bosch jackhammer', 'Specialized');

PRINT '';
PRINT 'INSERT INTO dbo.Equipment';
INSERT INTO dbo.Equipment
SELECT
    ins.Id,
    ins.Name,
    ins.Type
FROM @EquipmentToInsert ins
LEFT JOIN Equipment duplicate ON
    duplicate.Name = ins.Name
WHERE duplicate.EquipmentID IS NULL;

PRINT '';
PRINT 'SELECT * FROM dbo.Equipment';
SELECT * FROM dbo.Equipment;

-- HiLoNextHighValues table

DECLARE @HiLoValuesToInsert TABLE (
    EntityName NVARCHAR(255) NOT NULL,
    NextHigh INT NOT NULL
);

PRINT '';
PRINT 'INSERT INTO @HiLoValuesToInsert';
INSERT INTO @HiLoValuesToInsert VALUES
    ('Equipment', 1);

PRINT '';
PRINT 'INSERT INTO dbo.HiLoNextHighValues';
INSERT INTO dbo.HiLoNextHighValues
SELECT
    ins.EntityName,
    ins.NextHigh
FROM @HiLoValuesToInsert ins
LEFT JOIN HiLoNextHighValues duplicate ON
    duplicate.EntityName = ins.EntityName
WHERE duplicate.EntityName IS NULL;

PRINT '';
PRINT 'SELECT * FROM dbo.HiLoNextHighValues';
SELECT * FROM dbo.HiLoNextHighValues;
