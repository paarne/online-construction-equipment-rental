USE master;
GO

IF DB_ID(N'Rental') IS NULL
BEGIN
    CREATE DATABASE Rental
        COLLATE Estonian_CI_AS;
    -- Set to simple recovery model so that the transaction log won't grow
    ALTER DATABASE Rental SET RECOVERY SIMPLE;

    PRINT '';
    PRINT 'Database "Rental" created';
END
ELSE
BEGIN
    PRINT '';
    PRINT 'Database "Rental" already exists';
END

PRINT '';
PRINT 'Verify the database settings';
SELECT 
    db.name AS DB, 
    mf.physical_name AS Location,
    mf.size AS Pages, 
    CAST(mf.size*1.0/128 AS DECIMAL(10,2)) AS SizeMB,
    CASE mf.max_size 
        WHEN -1 THEN 'Unlimited'
        ELSE CAST(mf.max_size*8 AS VARCHAR(255)) + ' KB'
    END AS MaxSize,
    mf.growth/128 AS GrowthMB,
    mf.state_desc AS State,
    db.create_date AS CreationDate,
    db.compatibility_level AS CompatibilityLevel,
    CASE db.compatibility_level 
        WHEN 150 THEN 'SQL Server 2019'
        WHEN 140 THEN 'SQL Server 2017'
        WHEN 130 THEN 'SQL Server 2016'
        WHEN 120 THEN 'SQL Server 2014'
        WHEN 110 THEN 'SQL Server 2012'
        WHEN 100 THEN 'SQL Server 2008 (R2)'
        ELSE 'Unknown'
    END AS SqlServerVersion,
    db.collation_name AS Collation,
    db.user_access_desc AS UserAccessMode,
    db.is_read_only AS IsReadOnly,
    db.recovery_model_desc AS RecoveryModel
FROM sys.master_files mf
JOIN sys.databases db ON
    db.name = mf.name
WHERE db.name = N'Rental';
