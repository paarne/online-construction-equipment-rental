@echo off

sc query mssqlserver > nul
if %errorlevel% neq 0 (
    echo Could not find the SQL Server service. Please install it using the instructions in db/README.md.
    goto ANYKEY
)

echo Using elevated privileges
powershell Start-Process cmd -ArgumentList '/C', 'net start mssqlserver' -Verb RunAs -Wait
if %errorlevel% neq 0 (
   goto ANYKEY
)

echo:
echo SQL Server service has been started

:ANYKEY
echo Press any key to exit...
pause > nul
exit /b %errorlevel%
